# Employee Self Service (ESS)


<img src="./images/nl_logo.jpg" align="center" width="350px;" height="250px">

# GENERAL INFORMATION
Employee Self Service (ESS) is web-based applications that provide employees with access to their personal records and payroll details. ESS features allows administrative tasks like applying for a leave, reviewing of timesheet, requesting for overtime, and viewing of payslips.




# Directory Tab
Serves as a welcome screen after login. It contains list of information of employees who are currently active in the company. (Figure 1)
<img src="./images/directory.png">




# Personal Information Tab
Contains the information of the employee which includes basic information such as: (Figure 2)

  - Employee Name
  - Address
  - Government
  - Numbers and Etc
  - The Spouse Info
  - Parents Info

<img src="./images/personal.png">



# Employee Shift Schedules Tab
The employee shift schedules tab allows the employee to view thier assigned shift schedules in a calendar view. (Figure 3)
<img src="./images/shift_schedules.png">

How to use:

  - Located above the calendar are dropdown boxes for the month and year. Use this to navigate through the desired month of schedules.





# Daily Time Records Tab
Also kown as timesheet. In here, employees can view thier time logs (time in, time out) which was uploaded to the system. (Figure 4)
<img src="./images/dtr.png">

How to use:
  
  - Select the payroll period where you want to view your logs. Note: If no payroll period is shown in the dropdown, it is either your daily time record (logs) has not been processed yet or you are not part of the payroll.
  - The first four columns displays the date, your shift schedule assigned on that date, and your logs.
  - Shown in the last 6 columns are your leave credit consumed for that day, total hours rendered (Reg Hour), Late, Overtime (OT), and Possible Overtime (POT)
  - When overtime is applied, the OT hour will turn into color blue. Hover your mouse on it and it will display a breakdown of your overtime via tooltip.




# Leave Credits Tab
Displays the amount of leave credits available and the number of leave used by the employee. (Firgure 5)
<img src="./images/leave.png">




# Payslips Tab
Contains list of payroll period that the user was part of. It also allows the user to download payslips of finished or closed payrolls. (Figure 6)
<img src="./images/payslip.png">

How to use:
  
  - Every payroll that the employee was part of will be display in this tab. 
  - In the right part under the Actions column is the download button. This allows the employee to download his/her payslip on the selected payroll period.

>Note: Only closed payroll will be displayed. If a payroll period is still active, it will not be available in this tab for changes can still be made if a payroll is still active.





# ONLINE APPLICATIONS
Online applications are request forms that can be used by employees in applying for leave, overtime, etc. The ESS will send an email notification to the approver when an request has been made.


# Change Shift Schedules Tab
Allows the employee to request for a change shift schedule on a specific date within the current payroll period. (Figure 7)
<img src="./images/change_shift.png">

Hose to use:
  
  - To request for a change shift, click the Plus button located on the upper right of the panel. This will bring up a form. (Figure 7.1)

    - Shift schedule – select the desired shift schedule you want to request.
    - Credit Date – the date where you want the change shift to take effect.
    - Reason – the reason for your changing of shift.

  - Once you finished filling up the form, click the Add button to submit changes.
  - If you want to update your existing change shift application (ex. you want to change the credit date), you can click the Edit button. The first icon located under the Actions column. This will bring up the form with your existing change shift application for you to change it’s values.
  - Delete button - deletes the select application.
  - Resend button – Resends email to the approver.

<img src="./images/change_shift_form.png">




# Overtime Applications
Allows the employee to request for overtime. Contains other overtime applications the employee has requested. 
<img src="./images/overtime.png">

How to use:

  - Add button – located on the upper right corner of the panel. Used for adding Overtime Applications. This will bring up the modal form.

    - Credit date – the date of when you want your overtime.
    - Time start – the start time of your overtime. 
    - Time end – the time when you want your overtime to end.
    - Reason – your reason for applying for overtime (optional).

  - Time Format

 	  - use the 24 hour format for inputing time ex. for 6PM is 1800.
 	  - add a plus (+) sign before the time start or time end if the time falls to the next day. ex. if your overtime starts 9PM and ends in 2AM in the morning, your Time start is 2100 and Time End will be +0200
  - When done, click the Add button to submit changes.
  - Update button - This will bring up the form with your existing overtime application for you to change it’s values. Used for updating your overtime application.
  - Delete button - deletes the select application.
  - Resend button – Resends email to the approver.

<img src="./images/overtime_form.png">


# Leave Applications
Allows the employee to request for leave. (Figure 9)
<img src="./images/leave_app.png">

How to use:

  - To add a request for leave, click the Plus button on the upper right corner of the panel. This will bring up the modal form. Fill the form and once the finished, click the add button to submit changes.
  
    - Leave – select the type of leave you want to apply.
    - Date start – the date when you want your leave to start.
    - Date end – the end date of your leave. Note: if you apply for a 1 day leave, Date start and Date end should be the same.
    - Form Credit – determine the credit for your leave: Half day or auto, with or without credit. Default is Auto.

  - To update your request, click on the Update button located under the Actions column.
  - To delete you application, click on the Delete button located next to Update button.
  - To resend your applicatoin to the approver, click the Resend button.